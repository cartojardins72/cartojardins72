---
title: "A propos"
subtitle: "Les Jardins partagés au Mans"
date: 2018-04-30T10:05:49+10:00
images: ["img/jardins_partages.jpeg"]
draft: false
---

Les jardins partagés sont tout simplement des lieux accessibles soit librement
soit moyennant une petite participation pour pouvoir particulièrement y faire
pousser des légumes
ou des fruits.
